package ch.egli.streamlambda;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamLambda {
	public String path = "I:/Popcorn";
	public Stream<Path> stream;

	
	public void printAll() throws IOException {
		stream = Files.walk(Paths.get(path));
		stream.forEach(System.out::println);
	}
	
	public void skipLimit() throws IOException {
		stream = Files.walk(Paths.get(path));
		stream.skip(1).limit(5).forEach(System.out::println);
	}
	
	public void printSpecific() throws IOException {
		stream = Files.walk(Paths.get(path));
		stream.filter(x -> x.toString().contains("S.W.A.T") || x.toString().contains("Seal")).forEach(System.out::println);
	}
	
	public void countSpecific() throws IOException {
		stream = Files.walk(Paths.get(path));
		System.out.println(stream.filter(x -> x.toString().contains("S.W.A.T") || x.toString().contains("Seal")).count());
	}
	
	public void countLengthOfNames() throws IOException {
		AtomicInteger count = new AtomicInteger(0);
		stream = Files.walk(Paths.get(path));
		stream.forEach(x -> count.set(count.get() + x.toString().length()));
		System.out.println(count);
	}
	
	public void chainFileNames() throws IOException {
		String separator = "--";
		stream = Files.walk(Paths.get(path));
		System.out.println(stream.map(x -> x.toString()).collect(Collectors.joining(separator)));
	}
	
	public void filterMapCollect() throws IOException {
		stream = Files.walk(Paths.get(path));
		List<String> collection = stream.filter(x -> x.toString().length() > 3).map(x -> x.toString().substring(3, x.toString().length()))
				.collect(Collectors.toList());
		
		collection.forEach(System.out::println);
	}
}
