package ch.egli.exampledata;

public class Example1 {
	
	protected String string;
	protected Integer integer;
	protected Boolean bool;
	protected Double doubl;
	
	public Example1(String string){
		System.out.println(string);
	}
	
	@Deprecated
	public int doSomething(Integer one, Integer two){
		return one + two;
	}
	
	public void gurkensalat(){
		System.out.println("hehe gurkensalat ist super");
	}
	
}
