import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionDemo {
	public void analyzeClass() throws IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		String qualifiedName = "ch.egli.exampledata.Example1";
		Class<?> clazz;
		try {
			clazz = Class.forName(qualifiedName);
			System.out.println("Classname: " + clazz.getName());
			Object instance = clazz.getConstructor(String.class).newInstance("I am the constructor parameter."); //if theres only the default constructor, simply call clazz.newInstance()
			Method[] methods = clazz.getDeclaredMethods(); //getMethods also returns methods of super classes
			for(Method m : methods) {
				System.out.println("Method: " + m.getName());
				System.out.println("Modifiers: " + m.getModifiers());
				Annotation[] a =  m.getAnnotations();
				
				for(Annotation annotation : a) {
					System.out.println("Annotation: " + annotation.toString());
				}
			}
			
			Method addition = clazz.getMethod("doSomething", Integer.class, Integer.class);
			System.out.println(addition.invoke(instance, new Integer(1), new Integer(2)));
			
			Method gurkensalat = clazz.getMethod("gurkensalat");
			System.out.println(gurkensalat.invoke(instance));
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
