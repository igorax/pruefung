import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import ch.egli.streamlambda.StreamLambda;

public class AppLauncher {
	public static void main(String[] args) throws IOException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		StreamLambda sl = new StreamLambda();
		//sl.printAll();
		//sl.skipLimit();
		//sl.printSpecific();
		//sl.countSpecific();
		//sl.countLengthOfNames();
		//sl.chainFileNames();
		sl.filterMapCollect();
		
		//ReflectionDemo rd = new ReflectionDemo();
		//rd.analyzeClass();
	}
}
